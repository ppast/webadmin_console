<?php


\Ppast\Webadmin\Config\Root::setup('root', array(

	// URL EXTERNES		
	'LOG__NETWORK_TOOLS' => "https://myip.ms/info/whois/%",			// URL pour interrogation IP
	
	
	// CLONAGE SITE
	'CLONE__WWWROOT' => '/www',
	'CLONE__PHP_VERSIONS' => array('7.0', '7.1', '7.2', '7.3', '7.4', '8.0', '8.1', '8.2', '8.3'),
	'CLONE__DEFAULT_PHP_VERSION' => '8.2',
	'CLONE__COMPOSER_LIBC' => 'libc-webadmin/',


	// HTACCESS
	'HTA__BEGIN_TAG_IP_DENY' => '# LISTE IP BLOQUEES',						// tag de début pour liste des IP bloquées dans le .htaccess
	'HTA__END_TAG_IP_DENY' => '# /LISTE IP BLOQUEES',						// tag de fin pour liste des IP bloquées dans le .htaccess
	'HTA__BEGIN_TAG_UA_DENY' => '# INTERDIRE ROBOTS SELON USER-AGENT',		// tag de début pour liste des user-agents bloqués dans le .htaccess
	'HTA__END_TAG_UA_DENY' => '# /USER-AGENTS',								// tag de fin pour liste des user-agents bloqués dans le .htaccess

));

?>