<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe de base pour config site géré par la console
class ConsoleConfig extends Core
{
	// liste des sites gérés
	static $SITES = NULL;


	// faire la liste des sites, à partir du dossier de configuration
	static function listSites()
	{
		$sites = glob(self::$DATA_ROOT . 'config/*.php');
		$regs = [];
		if ( is_array($sites) )
			foreach ( $sites as $site )
				if ( preg_match('~config/(.*)\.php$~', $site, $regs) )
					include(self::$DATA_ROOT . 'config/' . $regs[1] . '.php');
	}
	
}


?>