<?php

// namespace
namespace Ppast\Webadmin\Config;



// classe de base pour config site géré par la console
class Site extends Base
{
	// méthode statique de construction : nom du profil et tableau associatif
	public static function setup($site, $params)
	{
		ConsoleConfig::$SITES[$site] = new Site($site, $params);
	}
}


?>