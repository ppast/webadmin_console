<?php

// namespace
namespace Ppast\Webadmin\WebadminConsoleCommands;


// [clauses use

// clauses use]



// RUN est statique, car appelé depuis CoreController, qui pour l'instant ne travaille qu'en statique.
// BODY est statique car défini dans RUN, statique.
// RENDER est appelé dynamiquement sur une instance (créer par Command::render), afin de pouvoir proposer de l'héritage de comportement : le traitement d'erreur est
// fait dans DefaultCommand, le traitement perso dans le classe fille.

class DefaultCommand extends \Ppast\Webadmin\Commands\Base
{
	// par défaut, ne rien faire
	protected function _renderDomainFromBody($domain, $data)
	{
	}
	
	
	// par défaut, ne rien faire
	protected function _renderCSS_JS()
	{
		// appel parent
		parent::_renderCSS_JS();
		
		?>
		<style>
		#webadmin_console_command_output .domain h1 {
			margin-bottom:10px;
			margin-top:30px;
		}
		
		#webadmin_console_command_output .domain_content .error {
			color:#d00;
			font-weight:bold;
		}
		
		#webadmin_console_command_output .domain_content {
			margin-left:20px;
			padding-left:5px;
			border-left:2px solid #999;
		}
		
		</style>
		<?php
	}
	
	
	// affichage
	protected function _renderBody()
	{
		// pour tous les websec_analyse_j_console__xxxx
		foreach ( $this->_body as $site )
		{
			// si erreur
			if ( is_string($site->body) )
			{
				?>
				<div class="domain">
					<h1><?php echo $site->site; ?></h1>
					<div class="domain_content">
						<div class="error"><?php echo $site->body; ?></div>
					</div>
				</div>
				<?php
			}
			
			
			// pas d'erreurs, les domaines sont dans le body (un site pourrait gérer plusieurs domaines)
			else
				foreach ( $site->body as $domain => $data )
				{
					?>
					<div class="domain">
						<h1><?php echo $domain; ?></h1>
						<div class="domain_content">
							<?php
							// rendu des data du domaine
							$this->_renderDomainFromBody($domain, $data);
							?>
						</div>
					</div>
					<?php
				}
		}
	}
	
	
	public function run()
	{
		/* extraire body 
		{
			websec_analyse_j_console__0 :
				{
					site : 'assistance-multimedia63.fr',
					body : 
						{
							site0_domain1 : 
								[
									{
										line : [ ... ],
										newip : true/false
									},
		
									{
										line : [ ... ],
										newip : true/false
									},
								],
								
							site0_domain2 :
								[
									...
								]
						},
				}
					
					
			websec_analyse_j_console__1 :
				{
					site : 'lesgrimoiresdestephane.fr',
					body :
						{
							site1_domain1 : 
								[
									{
										line : [ ... ],
										newip : true/false
									},
	
									{
										line : [ ... ],
										newip : true/false
									},
								]
						}
				}
		}
		*/
		return $this->status(true, "Commande exécutée.", json_decode($_REQUEST['body']));
	}
	
}


?>