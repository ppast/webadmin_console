<?php

// namespace
namespace Ppast\Webadmin\WebadminConsoleCommands;


// [clauses use
use \Ppast\Webadmin\Config\ConsoleConfig;
// clauses use]



class Websec_analyse_j_console extends DefaultCommand
{
	// par défaut, ne rien faire
	protected function _renderFooter()
	{
		// appel parent
		parent::_renderFooter();
		?>
		<div class="actions">
			<a href="javascript:void(0)" onclick="webadminConsole.analyse_j_console.interdire(); return false;">Interdire la sélection sur tous les sites</a>
		</div>
		<?php
	}
	
	
	// par défaut, ne rien faire
	protected function _renderCSS_JS()
	{
		// appel parent
		parent::_renderCSS_JS();
		?>
		<style>
		#webadmin_console_command_output .actions{
			margin-top:3em;
			margin-top:3em;
		}
		
		#webadmin_console_command_output table td input[type='text'] {
			width:115px;
		}

		</style>
		<script>
		window.webadminConsole = window['webadminConsole'] || {};
		webadminConsole.analyse_j_console = webadminConsole.analyse_j_console || {
		
			// réagir au clic dans la table
			clickHandler : function(e)
			{
				// déterminer cible du clic
				var target = nettools.jscore.getTarget(e);
				
				// si pris en charge (clic bouton)
				if ( (target.nodeName === 'INPUT') && (target.type === 'button') )
				{
					// remonter au champ de saisie
					var input = target;
					while ( input && (input.type != 'text') )
						input = input.previousSibling;

					switch ( target.value )
					{
						case '?' :
							window.open(('<?php echo ConsoleConfig::$ROOT_CFG->LOG__NETWORK_TOOLS; ?>').replace(/%/, input.value), 'network_tools');
							return false;
						
													
						case '++' :
							// autoriser uniquement si IP pas déjà connue
							if ( target.parentNode.parentNode.className.indexOf('highlight') >= 0 )
								webadminConsole.dispatch('cmd=websec_ip_update_console&ip=' + input.value);
						
							return false;
						
						
						default:
							return true; // pas géré
					}
				}
				else
					return true; // pas géré ici
			},
			
			
			// interdire la sélection 
			interdire : function()
			{
				// pour tous les blocs domaines, analyser les checkbox
				var cbs = document.querySelectorAll("#webadmin_console_command_output .domain input[type='checkbox']");
				var cbsl = cbs.length;
				var selected = [];
				
				// obtenir les cases cochées
				for ( var i = 0 ; i < cbsl ; i++ )
					if ( cbs[i].checked )
						selected.push(cbs[i].value);
					
				if ( selected.length && confirm('Interdire ces IPs sur tous les sites ?') )
					webadminConsole.dispatch('cmd=websec_ip_update_console&ip=' + selected.join(','));
			}
		
		};
		</script>
		<?php
	}
	
	
	// affichage body
	protected function _renderDomainFromBody($domain, $data)
	{
		?>
		<pre>
		<table onclick="return webadminConsole.analyse_j_console.clickHandler(event);" class="webadminList">
			<?php
			foreach ( $data as $line )
			{
				// si nouvel IP
				$class = $line->newip ? 'highlight':'';
				?>
				<tr>
					<td class="<?php echo $class; ?>"><?php
					$line->line[0] = '<div><input type="checkbox" value="' . $line->line[0] . '"> <input type="text"  readonly value="' . $line->line[0] . '"><input type="button" value="?"><input type="button" value="++"></div>';

					// convertir en cellules le tableau
					echo implode('</td><td>', $line->line);
					?></td>
				</tr>
				<?php
			}
			?>
		</table>
		</pre>
		<?php
	}
	
}


?>