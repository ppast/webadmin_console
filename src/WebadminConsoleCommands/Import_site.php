<?php

// namespace
namespace Ppast\Webadmin\WebadminConsoleCommands;


// [clauses use
use \Ppast\Webadmin\Config\ConsoleConfig;



class Import_site extends \Ppast\Webadmin\Commands\Base
{
	// par défaut, ne rien faire
	protected function _renderDomainFromBody($domain, $data)
	{
	}
	
	
	// affichage
	protected function _renderBody()
	{
		// obtenir fichier de config
		$fconfig = file_get_contents('http://' . rtrim($_REQUEST['s'], '/') . '/webadmin-console-setup');
		
		if ( $fconfig )
		{
			$f = fopen(ConsoleConfig::$DATA_ROOT . 'config/' . $_REQUEST['s'] . '.php', 'w');
			fwrite($f, $fconfig);
			fclose($f);
			?>
			<p>Configuration du site créée, <strong>il faut réactualiser la page complètement</strong>.</p>
			<?php
		}
		else
		{
			?>
			<p style="color:red; font-weight: bold;">Erreur à l'import de la configuration du site</p>
			<?
		}
	}
	
	
	public function run()
	{
		return $this->status(true, "Commande prête.");
	}
	
}


?>