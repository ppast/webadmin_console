<?php

// namespace
namespace Ppast\Webadmin\WebadminConsoleCommands;


// [clauses use
// clauses use]


// clauses use
use \Ppast\Webadmin\WebInterface\CoreController;
use \Ppast\Webadmin\Config\Core;



class Clone_site extends \Ppast\Webadmin\Commands\Base
{
	// par défaut, ne rien faire
	protected function _renderDomainFromBody($domain, $data)
	{
	}
	
	
	// par défaut, ne rien faire
	protected function _renderCSS_JS()
	{
		// appel parent
		parent::_renderCSS_JS();
		
		?>
		<style>
		#fCloneSite {
		}
		
		
		#fCloneSite input[name='domain'] {width:300px;}
		#fCloneSite input[name='ftpserver'] {width:400px;}
		</style>
		<script>
		window.webadminConsole = window['webadminConsole'] || {};
		webadminConsole.clone_site = {
		
			// afficher formulaire de clonage
			cloneForm : function(formname)
			{
				var form = document.getElementById(formname);
				nettools.ui.FormBuilder.createForm(
						{
							// target
							target : form,
							
							// champs
							fields :
								{
									domain : 	{type:'text', label:'Nom de domaine', placeholder:'domain.tld'},
									ftpserver:	{type:'text', label:'Serveur FTP', placeholder:'ftp.domain.tld ou ftp.clusterxxx.(hosting.)ovh.net', newLineBefore:true},
									ftpuser : 	{type:'text', label:'Utilisateur FTP', newLineBefore:true},
									ftppasswd :	{type:'text', label:'Mot de passe FTP'},
									wwwroot:	{type:'text', label:'Racine FTP hébergement', value:'<?php echo Core::$ROOT_CFG->CLONE__WWWROOT; ?>', newLineBefore: true, placeholder:'/www'},
									shortname: 	{type:'text', label:'Libellé court', value:'myApp', newLineBefore:true},
									loguser : 	{type:'text', label:'Utilisateur log', newLineBefore:true},
									logpasswd :	{type:'text', label:'Mot de passe log'},
									libc : 		{type:'text', label:'Dossier Composer webadmin à cloner', value:'<?php echo Core::$ROOT_CFG->CLONE__COMPOSER_LIBC; ?>', newLineBefore:true, placeholder:'libc-webadmin/'},
									output:		{type:'radio', items:[{name:'ftp', label:'Envoi par FTP', value:'ftp', checked:true}, {name:'download', value:'download', label:'Téléchargement ZIP'}], newLineBefore:true},
									phpversion:	{type:'select', label:'Version PHP', value:'<?php echo Core::$ROOT_CFG->CLONE__DEFAULT_PHP_VERSION; ?>', options:['<?php echo implode("','", Core::$ROOT_CFG->CLONE__PHP_VERSIONS); ?>'], newLineBefore:true},
								},
							
							// champ requis
							required : ['domain', 'ftpserver', 'ftpuser', 'ftppasswd', 'wwwroot', 'shortname', 'loguser', 'logpasswd', 'libc', 'phpversion'],
							
							// conformité champs
							regexps : {
								// https://www.debuggex.com/r/y4Xe_hDVO11bv1DV#
								domain : /^(([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\.([a-zA-Z]{2,6}|[a-zA-Z0-9-]{2,30}\.[a-zA-Z]{2,3})$/,
								ftpserver : /^(ftp\.(([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\.([a-zA-Z]{2,6}|[a-zA-Z0-9-]{2,30}\.[a-zA-Z]{2,3}))|(ftp\.cluster[0-9]+\.hosting\.ovh\.net)$/,
								wwwroot : /^\/[a-z0-9._-]+$/,
								libc : /^[a-z0-9-]+\/$/,
							},
							
							// submit
							submit : new nettools.jscore.SubmitHandlers.Post(
								{
									target : '<?php echo CoreController::$routage; ?>',
									csrf : true,
									data : {
										u : '<?php echo CoreController::$USER; ?>',
										cmd : 'do_clone_site'
									}
								})
						}
						
					);
			}
		
		};
		</script>
		<?php
	}
	
	
	// affichage
	protected function _renderBody()
	{
		?>
		<div style="background-color: #F8EFC1; color:black; padding:5px; border:2px solid black; margin-top:15px; margin-bottom:10px;"><strong>Pré-requis : </strong>
			<ul>
				<li>Avoir modifié le mot de passe FTP</li>
				<li>Avoir créé un utilisateur pour l'accès aux logs</li>
			</ul>
		</div>
		<form class="uiForm" id="fCloneSite"></form>
		<script>
		webadminConsole.clone_site.cloneForm('fCloneSite');
		</script>
		<?
	}
	
	
	public function run()
	{
		return $this->status(true, "Commande prête.");
	}
	
}


?>