<?php

// namespace
namespace Ppast\Webadmin\WebadminConsoleCommands;


// [clauses use
// clauses use]


use \Ppast\Webadmin\Config\Core;


class Do_clone_site extends \Ppast\Webadmin\Commands\Base
{
	// par défaut, ne rien faire
	protected function _renderCSS_JS()
	{
		// appel parent
		parent::_renderCSS_JS();
		
		?>
		<style>
		.console {
			padding:5px; 
			background-color:black; 
			font-family:Courier; 
			color:#FFF;	
		}
		</style>
		<?php
	}
	
		
	// remplacement dans un fichier
	protected function _replaceInFile($search, $replace, $file)
	{
		// remplacer domain.tld
		$body = file_get_contents($file);
		$body = str_replace($search, $replace, $body);
		$f = fopen($file, 'w');
		fwrite($f, $body);
		fclose($f);
	}
	
	
	// remplacer domain.tld dans un fichier
	protected function _replaceDomainInFile($domain, $file)
	{
		$this->_replaceInFile('domain.tld', $domain, $file);
	}
	
	
	// remplacer domain.tld dans plusieurs fichiers
	protected function _replaceDomainInFiles($domain, $files)
	{
		foreach ( $files as $f )
			$this->_replaceDomainInFile($domain, $f);
	}
	
	
	
	// formater une chaine et l'afficher dans une console noir et blanc
	static function consoleOutput($str)
	{
		$str = str_replace(array("\r\n","\n"), '<br>', $str);
		$html = <<<"HTML"
<div class="console">
$str
</div>
HTML;
		return $html;
	}
	
	
	// exécuter la commande
	public function run()
	{
		// racine du clonage et données
		$cloneroot = Core::$DATA_ROOT . 'site_clone/';
		$clonedata = Core::$DATA_ROOT . 'site_clone_data/';
		

		// cloner le dossier modèle
		$clonetmp = Core::$DATA_ROOT . 'site_clone_tmp/';
		$script = "mkdir -p {$clonetmp} ; cp -r {$cloneroot}* $clonetmp";
		$ret = trim(shell_exec($script));
		if ( $ret )
			return $this->status(false, 'Erreur à la création du clonage', self::consoleOutput($ret), true);

	
		// racine webadmin clonée
		$webadminclone = $clonetmp . 'webadmin/data/';
		

		// créer fichier config domaine par défaut
		$domain = $_REQUEST['domain'];
		if ( !rename($webadminclone . 'config_domains/domain.tld.php', $webadminclone . 'config_domains/' . $domain . '.php') )
			return $this->status(false, 'Erreur à la création de la configuration du domaine');

		
		// traiter remplacements du domain.tld
		$this->_replaceDomainInFiles($domain, 
				array(
					// htaccess public/
					$clonetmp . 'public/.htaccess',
				
					// config domaine
					$webadminclone . 'config_domains/' . $domain . '.php',
					
					// config profiles par défaut
					$webadminclone . 'config_profiles/librairies.php',
					$webadminclone . 'config_profiles/public.php',
					$webadminclone . 'config_profiles/webadmin.php',
					
					// webadmin console setup
					$clonetmp . 'webadmin-console-setup'
				)
			);
			
			
		// traiter remplacement variables
		$this->_replaceInFile('%loguser%', $_REQUEST['loguser'], $webadminclone . 'config_domains/' . $domain . '.php');
		$this->_replaceInFile('%logpasswd%', $_REQUEST['logpasswd'], $webadminclone . 'config_domains/' . $domain . '.php');
		$this->_replaceInFile('%shortname%', $_REQUEST['shortname'], $clonetmp . 'webadmin-console-setup');
			
			
		// extraire ips bloquées et remplacer
		$regs = [];
		if ( preg_match('{' . Core::$ROOT_CFG->HTA__BEGIN_TAG_IP_DENY . '(.*)' . Core::$ROOT_CFG->HTA__END_TAG_IP_DENY . '}s', file_get_contents(Core::$ROOT . '.htaccess'), $regs) )
			$this->_replaceInFile('# %blocked_ips%', $regs[1], $clonetmp . 'tmp.htaccess');

		// extraire user agents bloqués et remplacer
		$regs = [];
		if ( preg_match('{' . Core::$ROOT_CFG->HTA__BEGIN_TAG_UA_DENY . '(.*)' . Core::$ROOT_CFG->HTA__END_TAG_UA_DENY . '}s', file_get_contents(Core::$ROOT . '.htaccess'), $regs) )
			$this->_replaceInFile('# %blocked_uas%', $regs[1], $clonetmp . 'tmp.htaccess');
			
			
		// copier libc-webadmin depuis sources indiquée
		$libc = Core::$ROOT . trim($_REQUEST['libc'], '/') . '/';
		$script = "cp -r {$libc}* {$clonetmp}libc-webadmin";
		$ret = trim(shell_exec($script));
		if ( $ret )
			return $this->status(false, 'Erreur à la création du dossier Composer libc-webadmin', self::consoleOutput($ret), true);
			
			
		// chemin fichier temporaire zip
		$ztmp = sys_get_temp_dir() . "/{$domain}-" . uniqid() . '.zip';
	
		// script et exécution ; par défaut, "*" ne matche pas les fichiers temporaires. ensuite -r se charge de le faire
		// dans les sous-dossiers
		$script = "cd {$clonetmp} ; zip -r $ztmp *";
		shell_exec($script);


		// supprimer dossier tmp
		$script = "rm -r $clonetmp";
		$ret = trim(shell_exec($script));
		if ( $ret )
			return $this->status(false, 'Erreur à la suppression du dossier temporaire', self::consoleOutput($ret), true);


		// si téléchargement demandé
		if ( $_REQUEST['output'] == 'download' )
			if ( file_exists($ztmp) )
			{
				header("Content-Type: application/zip; name=\"{$domain}.zip\"");
				header("Content-Disposition: attachment; filename=\"{$domain}.zip\"");
				header("Expires: 0");
				header("Cache-Control: no-cache, must-revalidate");
				header("Pragma: no-cache"); 
		
				readfile($ztmp);
				unlink($ztmp);
				die();
			}
			else
				return $this->status(false, "Fichier temporaire du clonage introuvable");
		
		// si FTP demandé
		else
		{
			// connexion
			$cid = ftp_connect($_REQUEST['ftpserver']);
			if ( !$cid )
				return $this->status(false, "Erreur connexion au serveur FTP '{$_REQUEST['ftpserver']}'");

			if ( !ftp_login($cid, $_REQUEST['ftpuser'], $_REQUEST['ftppasswd']) )
				return $this->status(false, 'Erreur user/password connexion FTP');
				
			// activer mode passif (obligatoire)
			ftp_pasv($cid, true);
				
			// aller dans dossier www
			if ( !ftp_chdir($cid, '/' . trim($_REQUEST['wwwroot'], '/')) )
				return $this->status(false, 'Erreur accès dossier FTP racine' . $_REQUEST['wwwroot']);
				
			// envoyer le fichier
			if ( !ftp_put($cid, 'setup.zip', $ztmp, FTP_BINARY) )
				return $this->status(false, 'Erreur envoi FTP clone');

			
			// préparer fichier de données setup
			$setup = array(
					'phpversion'	=> $_REQUEST['phpversion'],
					'ovhconfig'		=> file_get_contents($clonedata . '.ovhconfig'),
					'todo'			=> file_get_contents($clonedata . 'todo.txt')
				);
				
			
			// chemin fichier temporaire setup à envoyer
			$setuptmp = sys_get_temp_dir() . "/setup-{$domain}-" . uniqid() . '.dat';
			$f = fopen($setuptmp, 'w');
			fwrite($f, serialize($setup));
			fclose($f);
			

			// envoyer le fichier de données setup
			if ( !ftp_put($cid, 'setup.dat', $setuptmp, FTP_BINARY) )
				return $this->status(false, 'Erreur envoi FTP setup.dat');
				
			unlink($setuptmp);

				
			// envoyer le fichier setup
			if ( !ftp_put($cid, 'setup.php', $clonedata . 'setup.php', FTP_TEXT) )
				return $this->status(false, 'Erreur envoi FTP setup.php');
				

			// fermer la connexion
			ftp_close($cid);
			
			// attendre
			sleep(1);
			$output = '<p><a href="http://' . $domain . '/setup.php" target="_blank">Effectuer le clônage en cliquant ici</a></p>';
				
				
			return $this->status(true, "Clônage préparé", $output, true);
		}
		
		
	}
	
}


?>