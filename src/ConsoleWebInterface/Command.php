<?php

// namespace
namespace Ppast\Webadmin\ConsoleWebInterface;



// clauses use
use \Ppast\Webadmin\WebInterface\CoreController;


class Command
{
	// affichage
	static function render($u)
	{
		?>
		<style>
		#webadmin_console_command_output{
			margin:5px;
		}
		</style>
		<div id="webadmin_console_command_output">
			<?php
			CoreController::$command->render();
			?>
		</div>
		<?php
	}
}

?>