<?php

// namespace
namespace Ppast\Webadmin\ConsoleWebInterface;



// clauses use
use \Ppast\Webadmin\Config\ConsoleConfig;
use \Ppast\Webadmin\Auth\AuthHandler;
use \Ppast\Webadmin\WebInterface\CoreController;



// controller interface console
class Controller extends CoreController
{
	// affichage
	static function process($data_root)
	{
		// appel parent
		CoreController::coreProcess($data_root, __FILE__);


		
		// faire l'inventaire des sites gérés par la console
		ConsoleConfig::listSites();

		


		// ========================
		// TRAITER AUTHENTIFICATION
		// ========================
		
		// si soumission formulaire identification
		if ( isset($_REQUEST['u']) && isset($_REQUEST['fLogin_password']) && isset($_REQUEST['fLogin_token']) && (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') )
			// authentifier l'utilisateur ; si pb, le script est arrêté dans authenticate() ; sinon, on récupère le token généré et mis en cookie
			self::$auth = AuthHandler::authenticateForm($_REQUEST['u'], hash('sha256',$_REQUEST['fLogin_password']), $_REQUEST['fLogin_token']);
		else if ( isset($_REQUEST['u']) && isset($_REQUEST['token']) )
			// sinon, si requete, authentifier selon jeton=sha256(cookie) en paramètre et cookie
			self::$auth = AuthHandler::authenticateRequest($_REQUEST);
		else
		{
			// sinon, arrivée sans token ; il peut s'agir de l'arrivée initiale sur la page de login, mais pas d'une mauvaise authentification (= FALSE)
			self::$auth = NULL;
			AuthHandler::halt();
		}


		// si erreur d'authentification (=FALSE, et non pas NULL), arrêter maintenant
		if ( self::$auth === FALSE )
			die("Erreur d'authentification");
			
			
		// si auth OK (et pas null, donc on ne peut pas se contenter du test précédent === FALSE)
		if ( self::$auth )
		{

			$params = array(NULL);
			$nscmd = '\\Ppast\\Webadmin\\WebadminConsoleCommands\\';
			$cmd = isset($_REQUEST['cmd'])?$_REQUEST['cmd']:'';


			// exécution de la commande et récup résultat booléen ok dans ST (message, feedback, autofeedback dans commande)
			CoreController::coreCommandHandler($nscmd, $cmd, $params);
			$command = CoreController::$command;
		}
		else
			$command = NULL;
		
		
        // output buffered
        ob_start();
        
        try {
            // affichage template
            Template::render(self::$USER, $command);
        }
        finally
        {
            ob_end_flush();
        }
		
	}
}

?>