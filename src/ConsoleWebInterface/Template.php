<?php

// namespace
namespace Ppast\Webadmin\ConsoleWebInterface;



// clauses use
use \Ppast\Webadmin\Config\Core;
use \Ppast\Webadmin\Auth\AuthHandler;
use \Ppast\Webadmin\WebInterface\CoreController;



// page
class Template 
{
	static public function render($u, \Ppast\Webadmin\Commands\Base $command = NULL)
	{
		$relativeroot = str_replace(Core::$ROOT, '', Core::$WEBADMIN_CORE_ROOT);
		$callerroot = str_replace(Core::$ROOT, '', Core::$CALLER_ROOT);
		$selfrelativeroot = str_replace(Core::$ROOT, '', Core::$WEBADMIN_ROOT);
		?>
		<!DOCTYPE html>
		<html>
		<head>
			<script src="/<? echo $relativeroot; ?>Includes/net-tools/js-core/js-core.min.js"></script>
			<script src="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.min.js"></script>
			<script src="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.desktop.min.js"></script>
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.min.css">
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.<?php echo (Core::$ROOT_USER_CFG->DARKTHEME ? 'dark':'yellow'); ?>-theme.min.css">
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.desktop.min.css">
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/net-tools/ui/ui.desktop.<?php echo (Core::$ROOT_USER_CFG->DARKTHEME ? 'dark':'yellow'); ?>-theme.min.css">
			<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/ui.min.css">
			<link rel="stylesheet" href="/<? echo $selfrelativeroot; ?>Includes/theme.min.css">
            <?php
            // gestion du thème sombre
            if ( Core::$ROOT_USER_CFG->DARKTHEME )
            {
                ?>
				<link rel="stylesheet" href="/<? echo $relativeroot; ?>Includes/ui.dark-theme.min.css">
                <link rel="stylesheet" href="/<? echo $selfrelativeroot; ?>Includes/dark-theme.min.css">
                <?php
            }
            ?>
            <title>Console d'administration centralisée</title>
			<link rel="icon" href="/<?php echo $callerroot; ?>favicon.ico"/> 
			<script>
			
			
			
			// initialiser CSRF
			nettools.jscore.SecureRequestHelper.setCSRFCookieName('<? echo AuthHandler::$CSRFCookieName; ?>');
			nettools.jscore.SecureRequestHelper.setCSRFSubmittedValueName('token');
			nettools.jscore.SecureRequestHelper.autoReplaceLinksWithPOSTRequests();
		
			</script>
			<?php
			// si authentifié, insérer CSS et JS nécessaires 
			if ( CoreController::$auth )
			{
				?>
				<script>
				window.webadminConsole = window['webadminConsole'] || (function(){
					
					// --- DECL. PRIVEES ---
					var _dispatchFlags = null;
					var _dispatchPromisesResolvers = null;
					var _dispatchResponses = null;
					var _dispatchCommandBody = null;
					var _progressObj = null;
					var _iframes = [];
					
					
					// forger une URL d'entrée, dans laquelle on incorpore un login crypté et un timestamp
					function _loginUrl(div)
					{
						var dt = new Date();
						dt = Math.floor(dt.valueOf()/1000) + 30;
						return nettools.jscore.appendToUrl(div.getElementsByTagName('a')[0].getAttribute('data-href'), "url_password=" + div.getAttribute('data-urlpasswd') + 
															"&url_ts=" + nettools.jscore.hmacSha256('ts!'+dt, atob('d2ViQWRtaW5BdXRofGF1dGhlbnRpY2F0ZVVybA==')) +
															dt.toString(16));
					}
					
					
					// forger une URL avec a
					function _url(a)
					{
						var token = a.parentNode.getAttribute('data-token');
						if ( token )
							return nettools.jscore.appendToUrl(a.getAttribute('data-href'), "token=" + token);
						else
						{
							alert("Aucun token d'identification obtenu pour ce site !");
							return "";
						}
					}
					
					
					// forger une URL avec token pour dispatch
					function _dispatchUrl(url, div)
					{
						var token = div.getAttribute('data-token');
						if ( token )
							return url;
						else
						{
							alert("Aucun token d'identification obtenu pour ce site !");
							return "";
						}
					}
					
					
					// interroger les sites gérés
					function _sitesStatus()
					{
						var divs = document.querySelectorAll('div#webadminconsole_sites div.site_tag');
						var head = document.getElementsByTagName('head')[0];
						var divsl = divs.length;
						
						// pour tous les sites gérés
						for ( var i = 0 ; i < divsl ; i++ )
							{
								var script = document.createElement('script');
								script.type = 'text/javascript';
								script.src = _loginUrl(divs[i]) + '&cmd=jsonp_site_status&i=' + i + '&jsonp=webadminConsole.sites_status_callback';
								head.appendChild(script);
							}
					}
					
					
					// traiter retour d'erreurs
					function _json_response_status(resp)
					{
						// si réponse sans erreur
						if ( !resp.status && resp.message )
							alert(resp.message);
							
						return resp.status;
					}
					
					
					// réceptionner un message d'une frame
					function _receiveMessage(event)
					{
						webadminConsole.commandNoBody(event.data);
					}
					
					
					// autoload callback
					nettools.jscore.registerOnLoadCallback(_sitesStatus);
					
					
					// attendre des évènements crossframes/crossorigns de dispatch de commandes
					window.addEventListener("message", _receiveMessage, false);
					
					// --- /DECL. PRIVEES ---
					
					return {
						
						// callback jsonp
						sites_status_callback : function(resp)
						{
							// data = {statut:true/false, message:'chaine', response:{...}}
							var divs = document.querySelectorAll('div#webadminconsole_sites div.site_tag');
							
							// si réponse sans erreur
							if ( _json_response_status(resp) )
							{
								// si commande fournie, il y a une réponse en cas de statut positif
								if ( resp.response )
								{
									// définir style selon sécurisation du site, et mémoriser token
									divs[resp.response.index].className = resp.response.secured ? 'site_tag site_secured':'site_tag site_unsecured';
									divs[resp.response.index].setAttribute('data-token', resp.token);
                                    
                                    // définir environnement
                                    var env = [];
                                    for ( var p in resp.response.env )
                                        env.push(p + ' = ' + resp.response.env[p]);
                                    
                                    
                                    // environnement
                                    var key = 'webadminconsole:' + divs[resp.response.index].querySelector('span.width_gt_1024').innerHTML;
                                    var jsonenv = JSON.stringify(env);

                                    // comparer environnement actuel avec valeur mémorisée précédemment
                                    if ( localStorage.getItem(key) != jsonenv )
                                    {
                                        divs[resp.response.index].getElementsByTagName('div')[1].className = 'alert';
                                        divs[resp.response.index].getElementsByTagName('div')[1].innerHTML = 'ENVIRONNEMENT MODIFIE';
                                    }
                                                                            
                                    // mémoriser environnement dans navigateur    
                                    localStorage.setItem(key, JSON.stringify(env));
                                        
                                    // affichage environnement
                                    divs[resp.response.index].getElementsByTagName('div')[0].innerHTML = "<a href=\"javascript:void(0)\" onclick=\"alert('" + env.join("\\n") + "'); return false;\">Infos. tech.</a>";//env.join('\n');
                                    
                                    
                                    // assertions
                                    if ( resp.response.ko_checks && resp.response.ko_checks.length )
                                    {
                                        var checksl = resp.response.ko_checks.length;
                                        divs[resp.response.index].getElementsByTagName('div')[1].className = 'alert';
                                        divs[resp.response.index].getElementsByTagName('div')[1].innerHTML = 'ERR PRE-REQUIS : ' + resp.response.ko_checks.join(', ');
                                    }
								}
							}
						},
						
						
						// callback au retour du dispatch  : tenir la promesse en appelant la fonction resolve à l'indice INDEX, 
						// avec en valeur la réponse
						dispatch_callback : function(resp)
						{
							// progression
							_progressObj.feedback(Math.round((++_dispatchResponses) * 100 / _dispatchPromisesResolvers.length));
											

							// tenir la promesse
							if ( resp.response )
								_dispatchPromisesResolvers[resp.response.index](resp);
							else
								// si réponse non conforme, traiter avec Alert si message contenu dedans
								_json_response_status(resp);
							
							//{"status":true,"message":null,"response":{"index":0,"domains":{"assistance-multimedia63.fr":null/string_erreur}},"token":"token"}
						},
						
						
						// exécuter sans réception body (UPDATE htaccess, users)
						commandNoBody : function(cmd_url)
						{
							// on dispatche et on obtient une promesse qui sera tenue lorsque tous les dispatch seront achevés
							var pall = webadminConsole.dispatch(cmd_url);
							pall.then(function()
									{
										// ces commandes n'ont pas de body, le traitement est terminé direct
										alert('Traitement terminé !');
									}
									
								// traitement d'ererur
								).catch(function(err)
										{
											alert(err);
										}
									);
						},
						
						
						// exécuter une commande (avec body : websec)
						command : function(u, cmd, params)
						{
							// dispatcher et obtenir une promesse globale, tenue lorsque tous les dispatch seront achevés
							var pall = webadminConsole.dispatch('u=' + u + '&cmd=' + cmd + (params?'&' + params:''), 
									
									// dans dispatch, on créer une promesse pour chaque site ; attacher un callback 
									// THEN à appeler pour chacune de ces promesse
									function(resp)
									{
										// resp:{status:true/false, message:'', response:{"index":0,"domains":NULL, "body":<<<json>>>}}
										// consigner réponse si OK, erreur sinon
										if ( resp.status )
											_dispatchCommandBody[cmd + '__' + resp.response.index] = {
													site : _dispatchFlags[resp.response.index].querySelector('span.width_gt_1024').innerHTML,
													body : resp.response.body
												};
										else
											_dispatchCommandBody[cmd + '__' + resp.response.index] = {
													site : _dispatchFlags[resp.response.index].querySelector('span.width_gt_1024').innerHTML,
													body : resp.message
												};
									}
								);
								
								
								
							// quand traitement de tous les dispatch fini, recharger avec les BODY en POST
							pall.then(
									function()
									{
										nettools.jscore.SecureRequestHelper.post(
												// url
												'<?php echo CoreController::$routage; ?>',
												
												// post
												{
													u : '<?php echo $u; ?>',
													cmd : cmd,
													body : JSON.stringify(_dispatchCommandBody)
												}
											);
									}
									
								// traitement d'erreur
								).catch(function(err)
										{
											alert(err);
										}
									);
							
						},
						
						
						// envoyer une requête à tous les sites gérés ; renvoyer une promesse tenue lorsque
						// traitement terminé ; possibilité de rejet (si requête déjà en cours)
						dispatch : function(cmd_url, cb)
						{
							// n'autoriser que si pas requête en cours
							if ( _dispatchFlags != null )
								return Promise.reject('Une requête est déjà en cours !');
							
							
							// prendre liste des sites gérés
							var divs = document.querySelectorAll('div#webadminconsole_sites div.site_tag');
							var head = document.getElementsByTagName('head')[0];
							var divsl = divs.length;
							
							// init
							var cbname = 'webadminConsole.dispatch_callback';
							
							// tableau des sites (tags DIV, pour modifier ultérieurement styles, au fur et à mesure achèvement)
							_dispatchFlags = divs;
							
							// compteur avancement
							_dispatchResponses = 0;
							
							// liste des promesses (utilisé pour faire Promise.all)
							var _dispatchPromises = [];
							
							// init des callback resolve pour chaque promesse, appelé par jsonp
							_dispatchPromisesResolvers = [];
							
							// init commandes avec body
							_dispatchCommandBody = {};
							
							
							// progression, et commencer affichage à 0 dès maintenant
							_progressObj = new nettools.ui.RequestFeedback();
							_progressObj.feedback(0);
							
							
							// pour tous les sites gérés, créer une promesse pour chaque site
							for ( var i = 0 ; i < divsl ; i++ )
							{
								var p = new Promise(
										// exécuteur
										function (resolve, reject)
										{
											// mémoriser le résolveur pour que le callback jsonp puisse y faire référence
											// l'exécuteur est appelé tout de suite, dans le constructeur de la promesse
											// donc I a la bonne valeur 
											_dispatchPromisesResolvers[i] = resolve;
										}
									);
								_dispatchPromises[i] = p;
								
									
								// enchainer : 1 promesse site tenue (cas général)
								var p2 = p.then(function(resp)
										{
											//{"status":true,"message":null,"response":{"index":0,"domains":{"assistance-multimedia63.fr":null/string_erreur}},"token":"token"}
											// affichage erreur xmlhttp (status = false, affichage message)
											var msg = "";
											
											// vérifier message d'erreur renvoyé pour un domaine particulier (et non au niveau de l'appel)
											for ( var dom in resp.response.domains )
											{
												var st = resp.response.domains[dom];
												if ( st )
													msg += 'Le domaine \'' + dom + '\' a rencontré une erreur : ' + st + "\n";
											}
												
											// traiter l'erreur ici, et pas dans un CATCH de promesse, car
											// on ne veut pas interrompre toute la chaîne	
											if ( msg )
												alert(msg);
				
											// modifier flag site
											_dispatchFlags[resp.response.index].className = _dispatchFlags[resp.response.index].className.replace(/[ ]?site_process/, '');
											
											
											// **** ATTENTION **** : renvoyer réponse pour chainage THEN ci-dessous
											return resp;
										}
									);
									
								
								// si callback perso, enchainer avec THEN supplémentaire pour promesse tenue
								if ( typeof cb === 'function' )
									p2.then(cb);
							}
							

							// attendre la fin de toutes les promesses pour achever le traitement
							var pall = Promise.all(_dispatchPromises).then(
									
									// quand traitement fini, réinitialiser
									function(bodies)
									{
										_dispatchResponses = null;
										_dispatchFlags = null;
										_progressObj.load();
									}
								);
														

							// pour tous les sites gérés
							for ( var i = 0 ; i < divsl ; i++ )
								{
									// préparer l'url du script : jeton, commande, n° du site et data
									var url = nettools.jscore.appendToUrl(divs[i].getElementsByTagName('a')[0].getAttribute('data-href'), "token=" + divs[i].getAttribute('data-token') + 
																				'&i=' + i + '&jsonp=' + cbname + '&' + cmd_url);
									var src = _dispatchUrl(url, divs[i]);
									if ( src === '' )
										return;


									// créer balise script
									var script = document.createElement('script');
									script.type = 'text/javascript';
									
									// indiquer que ce site est occupé
									_dispatchFlags[i].className = _dispatchFlags[i].className + ' site_process';
									
									script.src = src; 
									head.appendChild(script);
								}
								
							
							// renvoyer promesse
							return pall;
						},
						
							
						// charger l'URL dans l'iframe
						loadIFrame : function(a)
						{
							// cacher les autres iframes
							for ( var i in _iframes )
							{
								_iframes[i].style.visibility = 'hidden';
								_iframes[i].style.display = 'none';
							}
							
							

							// rechercher l'iframe dans le cache des iframes
							var ifrkey = btoa(a.getAttribute('data-href'));
							if ( !_iframes[ifrkey] )
							{
								// créer l'url et inclure le token obtenu précédemment par SSO
								var iframe = document.createElement('iframe');
								iframe.className = 'webadminconsole_iframe';
								iframe.src = _url(a);
								_iframes[ifrkey] = iframe;
								
								document.getElementById('site_iframe').appendChild(iframe);
							}
							
							
							
							// afficher cette iframe
							_iframes[ifrkey].style.visibility = 'visible';
							_iframes[ifrkey].style.display = 'flex';
							
								
							
							// cacher console output
							var commands_output = document.getElementById('webadmin_console_command_output');
							if ( commands_output )
							{
								commands_output.style.visibility = 'hidden';
								commands_output.style.display = 'none';
							}
							
							
							// afficher div contenant l'iframe
							var div = document.getElementById('site_iframe');
							if ( div )
							{
								div.style.visibility = 'visible';
								div.style.display = 'flex';
							}
						},
						
						
						// mettre en place nouveau site
						cloneSite : function()
						{
							nettools.jscore.SecureRequestHelper.post(
									// url
									'<?php echo CoreController::$routage; ?>',
									
									// post
									{
										u : '<?php echo $u; ?>',
										cmd : 'clone_site'
									}
								);
						},
						
						
						// importer config site
						importSite : function()
						{
							var site = '';
							if ( site = window.prompt('domain.tld du site ?', '') )
								nettools.jscore.SecureRequestHelper.post(
										// url
										'<?php echo CoreController::$routage; ?>',

										// post
										{
											u : '<?php echo $u; ?>',
											cmd : 'import_site',
											s : site
										}
									);
						}
					};
				})();

				</script>
				<?php
			}
			?>
			</head>
		
		<body>
		<?
		// si commande, détecter erreur
		if ( $command && !$command->getStatus() )
			echo $command->getMessage();
			
		
		// si pas encore authentifié
		if ( !CoreController::$auth )
			Login::render($u);
		else
		{
			// affichage choix site et iframe
			Top::render($u);
			
			// affichage iframe et commandes
			Iframe::render($u, empty($_REQUEST['cmd']) ? true : false);
			
			if ( isset($_REQUEST['cmd']) )
				Command::render($u);
		}
	}
}

?>