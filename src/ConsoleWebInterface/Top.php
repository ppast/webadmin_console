<?php

// namespace
namespace Ppast\Webadmin\ConsoleWebInterface;



// clauses use
use \Ppast\Webadmin\Config\ConsoleConfig;
use \Ppast\Webadmin\Config\Users;
use \Ppast\Webadmin\WebInterface\CoreController;




class Top
{
	// affichage
	static function render($u)
	{
		?>
		<div class="sites">
			<div id="webadminconsole_sites">
				<?php
				// afficher choix applications
				foreach ( ConsoleConfig::$SITES as $s )
				{
                    $uobject = Users::usersProvider()->listUsers()[$u];
                    $upasswd = $uobject ? $uobject->password : '';

					echo '<div data-urlpasswd="' . $upasswd . '" class="site_tag">';
                        echo '<a data-href="' . $s->SITE__PORT . '?u=' . $u . '" onclick="webadminConsole.loadIFrame(this); return false;">';
                            echo '<img src="' . $s->SITE__ICON . '" width="24" height="24" align="absmiddle">';
                            echo '<span class="width_lt_1024" title="' . $s->name . '">' . $s->SITE__SHORTNAME . '</span><span class="width_gt_1024">' . $s->name . '</span>';
                        echo '</a>';
                        echo '<div class="width_gt_1024">';
                        echo '</div>';
                        echo '<div class="width_gt_1024">';
                        echo '</div>';
					echo '</div>';
				}
				?>
			</div>

			<div class="actions">
			<a href="<?php echo CoreController::$routage; ?>?u=<?php echo $u; ?>" data-csrf="1">Actualiser</a>			
			&nbsp;&nbsp;|&nbsp;&nbsp;
			Analyse de la sécurité : 
				  <a href="javascript:void(0)" onclick="webadminConsole.command('<?php echo $u; ?>', 'websec_analyse_j_console', 'j=0'); return false;">Ce jour</a> 
				- <a href="javascript:void(0)" onclick="webadminConsole.command('<?php echo $u; ?>', 'websec_analyse_j_console', 'j=1'); return false;">J-1</a> 
				
			&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="javascript:void(0)" onclick="webadminConsole.cloneSite(); return false;">Mettre en place nouveau site</a> 
			-
			<a href="javascript:void(0)" onclick="webadminConsole.importSite(); return false;">Importer la configuration d'un nouveau site</a> 
			</div>
		</div>

		<?php
	}
}

?>